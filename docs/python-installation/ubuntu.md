# Installing Python bindings with pip on Ubuntu

The tutorial was verified on Ubuntu 18.04 and 20.04 distributions.

# Prerequisites

Installation using pip will build the Python bindings from source, so it requires build dependencies for dv-processing
to be available in the system. This is specific for different Ubuntu flavors.

**Bindings require Python >= 3.6**

## Ubuntu 18.04

```shell
sudo apt install software-properties-common
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo add-apt-repository ppa:inivation-ppa/inivation-bionic
sudo apt update
sudo apt install dv-processing gcc-10 g++-10
```

## Ubuntu 20.04

Install dv-processing, this ensures that all dependencies are in place:

```shell
sudo apt install software-properties-common
sudo add-apt-repository ppa:inivation-ppa/inivation
sudo apt update
sudo apt install dv-processing gcc-10 g++-10
```

## Ubuntu 22.04

Install dv-processing, this ensures that all dependencies are in place:

```shell
sudo apt install software-properties-common
sudo add-apt-repository ppa:inivation-ppa/inivation
sudo apt update
sudo apt install dv-processing gcc g++
```

# Install using pip

Make sure to install python dependencies prior to installing the package:

```shell
python -m pip install numpy
python -m pip install git+https://gitlab.com/inivation/dv/dv-processing@rel_1.5
```
