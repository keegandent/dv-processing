import time
import dv_processing as dv

# Open any camera
capture = dv.io.CameraCapture()

# Run the loop while camera is still connected
while capture.isRunning():
    # Read a batch of triggers from the camera
    triggers = capture.getNextTriggerBatch()

    # The method does not wait for data arrive, it returns immediately with
    # latest available data or if no data is available, returns a `None`
    if triggers is not None and len(triggers) > 0:
        # Print the time range of imu data
        print(f"Received imu data within time range [{triggers[0].timestamp}; {triggers[-1].timestamp}]")
    else:
        time.sleep(0.001)
