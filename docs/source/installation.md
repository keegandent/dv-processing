# Installation of dv-processing

The dv-processing library can be installed using different package management tools as well as installations from
source. Since it is a header-only library, it can be used as a submodule in other CMake C++ projects.

## C++

Installation of dv-processing headers is available on Linux (ubuntu), MacOS, and Windows platforms.

### Linux {fa}`linux`

Installation on Linux is supported on Ubuntu distributions through `apt` using the iniVation PPA. Follow the
instructions according to the version of Ubuntu you have.

#### Ubuntu 18.04 (apt)

```shell
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo add-apt-repository ppa:inivation-ppa/inivation-bionic
sudo apt-get update
sudo apt-get install dv-processing
```

#### Ubuntu 20.04 / 22.04 (apt)

```shell
sudo add-apt-repository ppa:inivation-ppa/inivation
sudo apt-get update
sudo apt-get install dv-processing
```

#### Fedora Linux

```shell
sudo dnf copr enable inivation/inivation
sudo dnf install dv-processing
```

#### Gentoo

A valid Gentoo ebuild repository is available [here](https://gitlab.com/inivation/gentoo-inivation/) over Git. The
package to install is 'dev-libs/dv-processing'.

The standard packages in the Gentoo ebuild repository already include all development files.

### MacOS (brew) {fa}`apple`

Please notice that MacOS installation requires clang>=13.2.1.

```shell
brew tap inivation/inivation
brew install libcaer --with-libserialport --with-opencv
brew install dv-processing
```

### Windows (VCPKG) {fa}`windows`

Windows installation is supported using the VCPKG package manager. This requires cloning of the VCPKG repository, you
can follow the official [quick start guide to get started](https://github.com/microsoft/vcpkg#quick-start-windows). The
library can be installed using `.\vcpkg.exe install dv-processing` when the VCPKG is downloaded and bootstrapped.

A short tutorial for installation, create a directory at `C:\src`, execute these commands there:

```shell
git clone https://github.com/microsoft/vcpkg
.\vcpkg\bootstrap-vcpkg.bat
.\vcpkg\vcpkg install dv-processing
```

### Installing as a git submodule

Library can be used in a CMake project by adding it as a git submodule. Add the library using git, call the command from
your project directory:

```shell
git submodule https://gitlab.com/inivation/dv/dv-processing.git thirdparty/dv-processing
```

This will add source code of the latest released version of dv-processing to a directory `thirdparty/dv-processing`. Now
you can enable it in your CMakeLists.txt:

```cmake
ADD_SUBDIRECTORY(thirdparty/dv-processing)

# link your targets against the library
TARGET_LINK_LIBRARIES(your_target
        dv::processing
        ...)
```

### Building and install from source

Manual build and installation from source is also possible. The library requires a C++20 compatible compiler and
installed dependencies.

Please follow the installation instructions available in the
[source code repository](https://gitlab.com/inivation/dv/dv-processing/) to install the library from source code.

## Python {fa}`python`

The dv-processing library is also available in python using python bindings. Since the underlying implementation remains
C++ and only exposes the API through python bindings, the performance of the provided methods remains unaffected while
using it in python. Installation using `pip` is the recommended, since it correctly handles system-wide, venv, and conda
installations.

### Pip

Installation over pip is possible, although it requires building the bindings from source. Depending on your
environment, please install all dependencies required to build the library from source. With the dependencies installed,
the bindings can be installed using pip:

```shell
python -m pip install numpy
python -m pip install git+https://gitlab.com/inivation/dv/dv-processing
```

```{note}
Pip installation is the only supported method for installation in a virtual python environment (conda / venv).
```

### Python bindings in Linux

System-wide installation from package managers is possible, please follow <a href="#linux">same instructions</a> as per
C++ installation, except the package for python is named `dv-processing-python`.
